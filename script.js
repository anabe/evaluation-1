// Code réalisé par Anaïs Bernard
// Fonction de vérification du nombre de caractères dans le champ de texte du formulaire
// val = la chaîne de caractères entrés par l'utilisateur
function validation_reason(val) {
  // length = renvoie la longueur (le nombre de caractères) de la chaîne
  // L'expression x < y renvoie vraie (True) si la condition est vraie, faux (false) sinon
  // On renvoit donc l'inverse de l'expression
  return !(val.length < 15);
}

// Cette fonction permet de vérifier qu'une option à été choisi dans la liste déroulante.
// Val représente la "value" de l'option choisie par l'utilisateur
// On a défini la value du choix invalide à 'no'
function validation_cat(val) {
  // val !== 'no' renvoie vrai si val est différent de 'no', sinon renvoie faux
  // on retourne cette valeur (celle renvoyer par val différent de 'no')
  return val !== 'no';
}

// Cette fonction vérifie si le formulaire est valide
// renvoie VRAI (true) si le formulaire est invalide, sinon elle renvoie FAUX (false)
function is_invalid() {
  // On vérifie si la liste déroulante ou le champ de texte est invalide (l'élément aura un "is-invalid" dans sa class)
  // S'il l'un ou l'autre est invalide on renvoie VRAI
  // Si les deux sont valident on renvoie FAUX
  return $("#selectCatFormValidator").hasClass("is-invalid") || $("#reason_to_adopt").hasClass("is-invalid");
}

// Cette fonction initialise les champs du formulaire
function initialise_form() {
  // On définit la valeur selectionné de la liste déroulante à "no"
  // Celà remettra la selection sur l'option "-- Selectionnez --"
  $("#selectCatFormValidator").val("no");

  // On définit la valeur du champs de texte à "" (chaine vide)
  // Ce qui correspond à effacer la chaine déja présente.
  $("#reason_to_adopt").val("");
}
// evite que le script soit appelé trop tôt
$(document).ready(function () {

  // On appel la fonction d'initialisation du formulaire
  // Lorsque l'on rafraichit la page, le formulaire conserve ses anciennes valeurs
  initialise_form($("#selectCatFormValidator").val());


  // lorsque le choix de l'utilisateur dans la liste déroulante est modifiée
  $("#selectCatFormValidator").change(function () {
    // On remet par défaut l'aspect de la liste déroulante (le cadre rouge erreur disparait)
    // On modifie la class de l'élément à la valeur initiale : "form-control"
    $("#selectCatFormValidator").attr("class", "form-control");
  });

  // Concert le bloc formulaire avec le champ texte
  // keyup = lorsqu'une touche du clavier est relâchée 
  // cette action ne se fait que lorsque la touche est relâchée
  $("#reason_to_adopt").keyup(function () {
    // Elle remet par défaut l'attribut "class" du formulaire
    $("#reason_to_adopt").attr("class", "form-control");
  });

    // Bouton envoyer du formulaire
    // click = déclanche l'action au moment du clique sur le bouton
    // e représente "event" qui permet de récupérer l'action par défaut
  $("#submit_form").click(function (e) {

    // e.preventDefault = annule l'action par défaut du bouton (valider et envoyer le formulaire)
    e.preventDefault();
    // validation_cat = appelle la fonction du même nom se trouvant plus haut dans le code
    // On passe la value de l'option choisie par l'utilisateur dans la liste déroulante
    // Si le choix est invalide (la fonction nous renvoie FAUX)
    if (!validation_cat($("#selectCatFormValidator").val())) {
      
      // On rajoute à la class de selecteur le "is-invalid" pour préciser que c'est invalide
      $("#selectCatFormValidator").attr("class", "form-control is-invalid");
    } else {
      // Sinon on remet la valeur par défaut (disparition du cadre rouge d'erreur)
      $("#selectCatFormValidator").attr("class", "form-control");
    }

    // validation_reason = appelle la fonction du même nom se trouvant plus haut dans le code
    // On passe la value du champ de texte (la chaîne de caractères écrite par l'utilisateur)
    // Si la chaîne est invalide (la fonction nous renvoie FAUX)
    if (!validation_reason($("#reason_to_adopt").val())) {
      // On rajoute à la class de selecteur le "is-invalid" pour préciser que c'est invalide
      $("#reason_to_adopt").attr("class", "form-control is-invalid");
    } else {
      // Sinon on remet la valeur par défaut (disparition du cadre rouge d'erreur)
      $("#reason_to_adopt").attr("class", "form-control");
    }

    // On appelle la fonction de vérification d'invalidité du formulaire
    // S'il est invalide
    if (is_invalid()) {
      // Tout ce qui est en dessous de return ne sera pas exécuter
      // Ici le return permet d'arrêter le script
      return;
    }
    // Si tout est bon (valide) on envoie le formulaire
    $("#formulaire").submit();
  })

  // Le formulaire est envoyé (submit) et est donc valid.
  // e représente "event" qui permet de récupérer l'action par défaut
  $("#formulaire").submit(function(e) {

    // On utilise des constantes pour sauvegarder temporairement les valeurs du formulaire
    // Cela permet de réaxéder rapidement à l'une ou l'autre valeur.
    const name = $("#selectCatFormValidator").val();
    const reason = $("#reason_to_adopt").val();
    // e.preventDefault = annule l'action par défaut
    e.preventDefault();

    // On commence par rendre invisble le formulaire pour qu'il soit remplacé par un message de confirmation
    $("#card_form").css("opacity", "0");
    $("#card_form").css("height", "0");
     // On modifie le html de l'élément message_form_ok pour qu'il soit personnalisé.
     // Exempele de message :"Vous venez d'adopter Sushi" plus un message pour dire que la demande est en cours et un remerciement 
     $("#message_form_ok").html("Vous venez d'adopter " + name + " Votre demande est en cours de traitement. Merci.");
    // On remet la visibilité du message de confirmation en modifiant le propriété CSS pour l'animation
    $("#card").css("opacity", "1");
    $("#card").css("height", "auto");

   });
  //  lorsque l'on clique sur le bouton avec l'id raz (remise à zéro) :
   $("#raz_form").click(function() {
    //  On réinitialise le formulaire
     initialise_form();
    //  On fait disparaitre le message de confirmation pour faire réapparaitre le formulaire
     $("#card").css("opacity", "0");
     $("#card").css("height", "0");
    //  On remet le formulaire visible
     $("#card_form").css("opacity", "1");
     $("#card_form").css("height", "auto");

   });
});
